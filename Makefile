
FLASK_APP := flaskr

FORMAT_LINT_DIRS := \
	flaskr/ \
	tests/ \

install:
	@# pip>=21.2 is needed for recursive dependencies, which .[dev] uses.
	@# https://hynek.me/articles/python-recursive-optional-dependencies/
	python3 -m pip install --upgrade pip setuptools
	python3 -m pip install -e .[dev]

build:
	python -m build --wheel

debug:
	flask --app $(FLASK_APP) run --debug

format:
	ruff format $(FORMAT_LINT_DIRS)

lint:
	ruff check $(FORMAT_LINT_DIRS)

.PHONY: init-db
init-db:
	flask --app $(FLASK_APP) init-db

test:
	pytest -v

.PHONY: coverage
coverage:
	coverage run -m pytest -v
	coverage report

.PHONY: coverage-html
coverage-html: htmlcov/index.html
	chromium $<

htmlcov/index.html:
	coverage html

.PHONY: generate-secret-key
generate-secret-key:
	@python3 -c 'import secrets; print(secrets.token_hex())'
